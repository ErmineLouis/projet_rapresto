<?php session_start(); 

//$_SESSION['current_user'] = null;

?>

<!DOCTYPE html>
<html>
    <head>
        <title>RapResto</title>
        <meta charset="utf-8" />
        <link
        href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" 
        rel="stylesheet">
    <link href="tests/styles/style.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    <link rel= "icon" href="tests/image/logoraprestomini.png">


    </head>
    <body>
        <?php include_once('tests/header.php'); ?>

        <?php 

        $request = $_SERVER['REQUEST_URI'];

        switch ($request) {
            case '/' :
                require __DIR__ . '/tests/home.php';
                break;
            case '/inscription' :
                require __DIR__ . '/tests/inscription.php';
                break;
            case '/plats' :
                require __DIR__ . '/tests/plats.php';
                break;
            case '/connexion' :
                require __DIR__ . '/tests/connexion.php';
                break;
            case '/about' :
                require __DIR__ . '/tests/apropos.php';
                break;
            case '/ajoutplat' :
                require __DIR__ . '/tests/ajoutplat.php';
                break;
            case '/commande' :
                require __DIR__ . '/tests/commande.php';
                break;

            case '/mescommandes' :
                require __DIR__ . '/tests/mescommandes.php';
                break;
            default:
                http_response_code(404);
                require __DIR__ . '/tests/error.php';
                break;

            }


        
        
        ?>



        <?php include_once('tests/footer.php'); ?>
        
    </body>
</html>