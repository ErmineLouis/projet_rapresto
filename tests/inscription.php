<br>
<div class="container">

    <h1>Inscription</h1>


    <div class="mx-auto" style="width: 750px;">
        <div>
            <br>
            <br>
            <form action="/inscription" , method="post">

                <legend>
                    Informations personnelles

                    <i class="bi bi-person-check-fill" style="font-size: 30px; margin-left: 8px; color: #e4c93d;"></i>
                </legend>



                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Prenom*</span>
                    </div>

                    <input id="prenom" type="text" name="prenom" class="form-control" placeholder="Ex : Louis" required>
                </div>
                <div class="erreur" id="erreur2"></div>
                <div class="input-group mb-3">

                    <div class="input-group-prepend">
                        <span class="input-group-text">Nom*</span>
                    </div>

                    <input id="nom" type="text" class="form-control" name="nom" placeholder="Ex : Dupond" required>
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Date de naissance*</span>
                    </div>
                    <input id="datebirthday" type="date" name="date" class="form-control">
                </div>

                <div class="input-group mb-3">



                    <span class="input-group-text">Type d'utilisateur*</span>

                    <div class="form-check">
                        <input id="client" value="client" name="type" type="radio" onclick="displayHide()" style="margin-left: 10px" >
                        <label for="client">Client</label>
                    </div>
                    <div class="form-check">
                        <input id="restaurateur" value="restaurateur" name="type" type="radio" onclick="displayShow()" style="margin-left:10px" checked>
                        <label for="restaurateur">Restaurateur</label>
                    </div>


                </div>

                <div class="input-group mb-3" id="resto">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Nom du restaurant*</span>
                    </div>

                    <input id="resto" type="text" name="resto" class="form-control">
                </div>


                <script>
                    function displayHide() {
                        document.getElementById("resto").style.visibility = "hidden";
                    }

                    function displayShow() {
                        document.getElementById("resto").style.visibility = "visible";
                    }
                </script>


                <br>

                <legend>Votre adresse
                    <i class="bi-house-fill" style="font-size: 30px; margin-left: 8px; color: #e4c93d;"></i>

                </legend>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Adresse*</span>
                    </div>
                    <input id="adresse" type="text" name="adr" class="form-control" placeholder="Ex : 16 allee des oliviers" required>
                </div>

                <div class="erreur" id="erreur5"></div>

                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Code postal*</span>
                    </div>
                    <input id="codepostal" type="number" name="code" class="form-control" required>
                </div>

                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Ville*</span>
                    </div>

                    <input id="ville" type="text" name="city" class="form-control" required>

                </div>
                <legend>Informations de connexion :

                    <i class="bi bi-check-circle-fill" style="font-size: 30px; margin-left: 8px; color: #e4c93d;"></i>
                </legend>

                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Email*</span>
                    </div>
                    <input id="email" type="email" name="email" class="form-control" required>
                </div>

                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Mot de passe* </span>
                    </div>
                    <input id="mot_de_passe" type="password" name="password" class="form-control" required>
                </div>





                <span id="condition">* Champs obligatoires</span>
                <br>
                <br>

                <div class="text-center">
                    <button class="btn" id="boutonreset" type="reset">Réinitialiser</button>
                    <button class="btn" id="boutonvalid" type="submit" name="submit">Valider</button>
                </div>
                <br>
            </form>

        </div>
    </div>
</div>
<br>

<?php

if (isset($_POST['submit'])) {




    $nom = $_POST['nom'];
    $prenom = $_POST['prenom'];
    $date = $_POST['date'];
    $type = $_POST['type'];
    $adresse = $_POST['adr']." ".$_POST['code']." ".$_POST['city'];

    $login = $_POST['email'];
    $password = $_POST['password'];
    $nomresto = $_POST['resto'];








        try {
            $db = new PDO('mysql:host=localhost;dbname=rapresto_db;charset=utf8', 'root', 'root');
            
        } catch (Exception $e) {
            die('Erreur : ' . $e->getMessage());
        }

        $sqlQuery = 'INSERT INTO personne(login, mot_de_passe, nom, prenom, adresse, date_de_naissance, type, nom_resto) VALUES (:login, :mot_de_passe, :nom, :prenom, :adresse, :date_de_naissance, :type, :nom_resto)';

        $insertPersonne = $db->prepare($sqlQuery);


        $insertPersonne->execute([
            'login' => $login,
            'mot_de_passe' => $password,
            'nom' => $nom,
            'prenom' => $prenom,
            'adresse' => $adresse,
            'date_de_naissance' => $date,
            'type' => $type,
            'nom_resto' => $nomresto
        ]);
}

?>
<?php

for($i = 0 ; $i < 13 ; $i++) {

    echo '<br>';


}

?>