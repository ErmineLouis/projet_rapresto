<body>

  <nav id="nav" class="navbar navbar-expand">
    <a href="/">
      <img id="imgbrand" src="../tests/image/logorapresto.png" width="265" height="125">
    </a>

    <ul class="navbar-nav me-auto mb-2 mb-lg-2 white">

      <li class="nav-item" id="item1">
        <a class="nav-link active" aria-current="page" href="/"><span id="accueil">Accueil</span></a>
      </li>
      <li class="nav-item" id="item2">
        <a class="nav-link active" href="plats"><span id="plats">Nos plats</span></a>
      </li>
      <li class="nav-item" id="item3">
        <a class="nav-link active" href="about"><span id="about">A propos</span></a>
      </li>

      <?php


      if(!$_SESSION['current_user'] == null) {
       
          if ($_SESSION['current_user']['type'] == 'restaurateur') {

            echo'<li class="nav-item" id="item4">
            <a class="nav-link active" href="ajoutplat"><span>Mes plats</span></a>
          </li>';

          } else if($_SESSION['current_user']['type'] == 'client'){

            echo'<li class="nav-item" id="item4">
            <a class="nav-link active" href="mescommandes"><span>Mes commandes</span></a>
           </li>';


          }
        }
       
      ?>



    </ul>

    <?php

    if (isset($_POST['button'])) {
      $_SESSION['current_user'] = null;
    }


    if (isset($_SESSION['current_user'])) {

      echo '<form action="/" method="post">
  
      <button type="submit" class="btn " id="co" type="submit" name="button" style="color:#e4c93d;margin-right:35px;">Déconnexion</button></form>';
    } else {


      echo '<a href="/connexion"><button class="btn" id="co" style="color:#e4c93d;margin-right:35px;">Connexion</button></a>';
    }


    ?>



  </nav>
</body>