<div class="container">
    <h1>Historique de vos commandes</h1>

    <br>

<?php

try {
    $mysqlClient = new PDO('mysql:host=localhost;dbname=rapresto_db;charset=utf8', 'root', 'root');
} catch (Exception $e) {
    die('Erreur : ' . $e->getMessage());
}

$client = $_SESSION['current_user']['login'];

$sqlquery = "SELECT * FROM commande WHERE client = :client";

$preparedStatement = $mysqlClient->prepare($sqlquery);
$preparedStatement->execute(['client' => $client
]);
$res = $preparedStatement->fetchAll();


if(count($res) == 0) {

    echo '<div class="text-center"><h3>Vous n\'avez toujours rien commander</h3></div>';
} else {

   echo '<table class="table">';
   echo '<thead> <tr>
         <th scope="col">Nom du plat</th>
         <th scope="col">Quantité</th>
         <th scope="col">Date</th>
         <th scope="col">Informations</th>
         <th scope="col">Prix Total</th></tr></thead>';

    foreach($res as $temp) {

    

        $sqlqueryfinal = "SELECT * FROM plat, temp_plat_commande WHERE ref = id_plat AND id_commande = :id";

        $preparedStatementFinal = $mysqlClient->prepare($sqlqueryfinal);

        $preparedStatementFinal->execute(['id' => $temp['id']]);
        $resultat = $preparedStatementFinal->fetch();

        echo '<tbody><tr>
                <td>'.$resultat['nom'].'</td>
                <td>'.$temp['quantité'].'</td>
                <td>'.$temp['date'].'</td>
                <td>'.$temp['infos'].'</td>
                <td>'.$temp['prix_total'].'</td></tr></tbody>';


    }

    echo '</table>';




}



?>

<div class="text-center">
        <a href="/commande"><button class="btn">Passer Commande</button></a>
    </div>

    <br>

</div>