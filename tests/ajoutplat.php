<div class="container">
    <h1>Ajouter un plat</h1>


    <div class="mx-auto" style="width: 750px;">
        <div>
            <br>
            <br>
            <form action="/ajoutplat" , method="post">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Nom du plat</span>
                    </div>
                    <input id="nom" type="text" name="nom" class="form-control" required>
                </div>

                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Type du plat : </span>
                    </div>
                    <select name="type" id="type" class="form-select" required>
                        <option value="indien">Indien</option>
                        <option value="français">Français</option>
                        <option value="japonais">Japonais</option>
                        <option value="italien">Italien</option>
                        <option value="americain">Americain</option>
                        <option value="coréen">Coréen</option>
                        <option value="espagnol">Espagnol</option>
                        <option value="anglaise">Anglaise</option>
                        <option value="chinois">Chinois</option>

                    </select>
                </div>
                <div>
                    <label for="description">Description</label>

                        <textarea id="description" type="text" name="description" class="form-control" rows="3"></textarea>
                </div>
                <br>

                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Le prix</span>
                    </div>
                    <input id="prix" type="number" step=".01" name="prix" class="form-control" required>
                </div>

                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Frais de livraison</span>
                    </div>
                    <input id="frais" type="number" name="frais" step=".01" class="form-control" required>
                </div>

                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">URL pour l'illustration (<a href="https://media.istockphoto.com/photos/delicious-vegetarian-pizza-on-white-picture-id1192094401?k=20&m=1192094401&s=612x612&w=0&h=jesvXuPyvqM36GQ5QEvJrL3QZjK6YKsziUUF3ZbW0gw=">Exemple</a>)</span>
                    </div>
                    <input id="url" type="text" name="url" class="form-control" required>
                </div>


                <div class="text-center">
                    <button class="btn" id="boutonreset" type="reset">Réinitialiser</button>
                    <button class="btn" id="boutonvalid" type="submit" name="submit">Valider</button>
                </div>



            </form>

        </div>
    </div>
</div>
<br>

</div>

<?php

if (isset($_POST['submit'])) {

    $type = $_POST['type'];
    $nom = $_POST['nom'];
    $description = $_POST['description'];
    $prix = $_POST['prix'];
    $frais = $_POST['frais'];
    $img = $_POST['url'];
    $resto = $_SESSION['current_user']['login'];
    $ref = uniqid();

    try {
        $db = new PDO('mysql:host=localhost;dbname=rapresto_db;charset=utf8', 'root', 'root');
        
    } catch (Exception $e) {
        die('Erreur : ' . $e->getMessage());
    }

    

    $sqlQuery = 'INSERT INTO plat(ref, type,  nom, description, prix, frais_de_livraison, imgsrc, restaurateur) VALUES (:ref, :type, :nom, :description, :prix, :frais_de_livraison, :imgsrc, :restaurateur)';

    $insertPlat = $db->prepare($sqlQuery);



    $insertPlat->execute([
        'ref' => $ref,
        'type' => $type,
        'nom' => $nom,
        'description' => $description,
        'prix' => $prix,
        'frais_de_livraison' => $frais,
        'imgsrc' => $img,
        'restaurateur' => $resto
    ]);
}

for($i = 0 ; $i < 13 ; $i++) {

    echo '<br>';


}

?>