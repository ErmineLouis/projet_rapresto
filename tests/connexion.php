<br>

<div class="row text-center">
    <br>
    
    <h1> Connexion </h1>
    <div class="col-md-3"></div>
    <div class="col-md-6">
        <form action="/connexion" method="post">
            <div id="erreuremail" style="color:red;"></div>

            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Email</span>
                </div>
                <input id="email" type="email" name="email" class="form-control" required>
            </div>
            <div id="erreurmdp" style="color:red;"></div>

            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Mot de passe </span>
                </div>

                <input id="mot_de_passe" type="password" name="password" class="form-control" required>
            </div>
            <button class="btn" type="reset">Réinitialiser</button>

            <button class="btn" type="submit" name="submit">Connexion</button>

        </form>
    </div>
    <div class="col-md-3">

    </div>

</div>

<br>
<br>

<?php 

if (isset($_POST['submit'])) {

    $login = $_POST['email'];
    $password = $_POST['password'];

    try {
        $db = new PDO('mysql:host=localhost;dbname=rapresto_db;charset=utf8', 'root', 'root');
        
    } catch (Exception $e) {
        die('Erreur : ' . $e->getMessage());
    }

    $sqlQuery = 'SELECT * FROM personne WHERE login = :login';
    $preparedStatement = $db->prepare($sqlQuery);
    $preparedStatement->execute([
        'login' => $login
    ]);

    $res = $preparedStatement->fetch();
    if(empty($res)) {

        echo '<script> document.getElementById("erreuremail").innerHTML = "Email inconnue" </script>';

        
    } else {

        if($res['mot_de_passe'] == $password) {

            $_SESSION['current_user'] = $res;
            echo "<script>alert(\"Vous êtes bien connecté\")</script>";
            

        } else {

            echo '<script> document.getElementById("erreurmdp").innerHTML = "Mauvais mot de passe" </script>';

        }

        
    }

} 

?>