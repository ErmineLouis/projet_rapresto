<?php
try {
    $mysqlClient = new PDO('mysql:host=localhost;dbname=rapresto_db;charset=utf8', 'root', 'root');
} catch (Exception $e) {
    die('Erreur : ' . $e->getMessage());
}

$sqlQuery = 'SELECT * FROM plat';
$preparedStatement = $mysqlClient->prepare($sqlQuery);
$preparedStatement->execute();
$plats = $preparedStatement->fetchAll();

?>


<div class="container">


    <h1>Passez Commande</h1>


    <div class="mx-auto" style="width: 750px;">

        <br>
        <br>
        <form action="/commande" , method="post">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Le plat</span>
                </div>

                <select name="leplat" id="leplat" class="form-select" required>


                    <?php foreach ($plats as $plat) {

                        $nom = $plat['nom'];
                        $ref = $plat['ref'];

                        echo "<option value=".$ref.">$nom</option>";
                    }

                    ?>

                </select>
            </div>

            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Quantité</span>
                </div>
                <input id="quantité" type="number" name="quantité" class="form-control" required>

            </div>
            <div>

                <label for="infos">Informations complémentaires (allergies, compléments d'adresses, ...)</label>

                <textarea id="infos" type="text" name="infos" class="form-control" rows="3"></textarea>
            </div>


            <br>

            <div class="text-center">
                <button class="btn" id="boutonreset" type="reset">Réinitialiser</button>
                <button class="btn" id="boutonvalid" type="submit" name="submit">Valider</button>
            </div>



        </form>

    </div>

</div>
<br>


<?php

if (isset($_POST['submit'])) {


    $plat = $_POST['leplat'];
    $quantité = $_POST['quantité'];
    $infos = $_POST['infos'];
    $date = date('Y-m-d', time());


    try {
        $mysqlClient = new PDO('mysql:host=localhost;dbname=rapresto_db;charset=utf8', 'root', 'root');
    } catch (Exception $e) {
        die('Erreur : ' . $e->getMessage());
    }
    
    $sqlQuery = 'SELECT * FROM plat WHERE ref = :ref';
    $preparedStatement = $mysqlClient->prepare($sqlQuery);
    $preparedStatement->execute(['ref' => $plat]);
    $leplat = $preparedStatement->fetch();
    $id = uniqid();
    
    
    $prix = $leplat['prix'] * $quantité + $leplat['frais_de_livraison'];

 
    $client = $_SESSION['current_user']['login'];


    $sqlQueryfinal = 'INSERT INTO commande(id, prix_total, date, infos, quantité, client) VALUES (:id, :prix_total, :date, :infos, :quantite, :client)';
    $preparedStatementfinal = $mysqlClient->prepare($sqlQueryfinal);
    $preparedStatementfinal->execute([
    'id' => $id,
    'prix_total' => $prix,
    'date' => $date,
    'infos' => $infos,
    'quantite' => $quantité,
    'client' => $client
    ]);

    $sqlQueryfinal2 = 'INSERT INTO temp_plat_commande( id_plat, id_commande) VALUES ( :id_plat, :id_commande)';
    $preparedStatementfinal2 = $mysqlClient->prepare($sqlQueryfinal2);
    $preparedStatementfinal2->execute([
        'id_plat' => $plat,
        'id_commande' => $id
    ]);

    

}



?>
