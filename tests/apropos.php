<div class="container">
    <br>
    <h1>A propos de Rapresto</h1>
    <br>
    <div class="row">
        <div class="col-sm">
           
            <span style="color:#e4c93d; font-size:25px;">Faites votre choix</span>
            <p>Rapresto rassemble des centaines de restaurants. Lorsque vous ouvrez l'application, vous pouvez les faire défiler en quête d'inspiration, mais aussi rechercher un restaurant ou un type de cuisine si vous savez ce dont vous avez envie. Vous avez trouvé un plat qui vous plaît ? Il vous suffit d'appuyer dessus pour l'ajouter à votre commande.</p>
        </div>
        <div class="col-sm">
            <span style="color:#e4c93d; font-size:25px;">Commandez</span>
            <p>Au moment du paiement, vous verrez votre adresse, l'heure de livraison estimée et le prix de la commande (taxes et frais de livraison inclus). Si tout vous semble correct, appuyez sur Commander : c'est aussi simple que ça ! Vous pouvez utiliser les mêmes moyens de paiement que pour les courses avec Rapresto.</p>
        </div>
        <div class="col-sm">
            <span style="color:#e4c93d; font-size:25px;">Suivez votre commande</span>
            <p>Suivez votre commande dans l'application. Vous pourrez d'abord voir que le restaurant accepte la commande et commence à la préparer. Ensuite, lorsqu'elle sera quasiment prête, un coursier à proximité se dirigera vers le restaurant (en voiture, à vélo ou à scooter selon votre région) pour la prendre en charge. Enfin, il se rendra à votre adresse. Vous pourrez voir son nom et sa photo, ainsi que sa progression sur la carte.</p>

        </div>
    </div>
    <br>
</div>
